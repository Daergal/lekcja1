#!/usr/bin/env python

__version__ = '1.1'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

age = int(21)
print(age)

height = float(1.86)
print(height)

name = str("Tomek Roman")
print(name)
